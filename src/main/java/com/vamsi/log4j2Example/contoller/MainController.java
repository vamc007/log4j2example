package com.vamsi.log4j2Example.contoller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
	
	private static final Logger logger = LogManager.getLogger(MainController.class);
	
	@RequestMapping("/")
	public String index() {
		logger.error("error message");
		logger.debug("Debug message");
		logger.error("error message");
		return "Hello World!";
	}
}
